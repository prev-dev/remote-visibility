# Start from the latest golang base image
FROM golang:latest as builder

# Add Maintainer Info
LABEL maintainer="Paul Revello <paul@revello.dev>"

# Set the Current Working Directory inside the container
WORKDIR /app

# Copy go mod and sum files
COPY src/main.go src/go.mod ./

# Download all dependencies. Dependencies will be cached if the go.mod and go.sum files are not changed
RUN go get . 

# Build the Go app
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o showme .

######## Start a new stage from scratch #######

FROM gcr.io/distroless/base
USER nonroot
COPY --from=builder /app/showme /
CMD ["./showme"]
