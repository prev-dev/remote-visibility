package main

// gin is used to handle the routing and JSON
import (
	"github.com/gin-gonic/gin"
	"io"
	"log"
	"net"
	"net/http"
)

// clientInfo is useful to understand what IP or header information
// is being manipulated between the client and server
func clientInfo(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"clientIP":       c.ClientIP(),
		"requestHeaders": c.Request.Header,
	})
}

// we default to checking the A record here
func hostLookup(c *gin.Context) {
	var json Hostname
	if err := c.ShouldBindJSON(&json); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	ips, err := net.LookupIP(json.Hostname)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err,
		})
		panic(err)
	}
	c.JSON(http.StatusOK, gin.H{
		json.Hostname: ips,
	})
}

func urlCheck(c *gin.Context) {
	var json Site
	if err := c.ShouldBindJSON(&json); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	resp, err := http.Get(json.Address)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err,
		})
		panic(err)
	}

	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {

		}
	}(resp.Body)
	bodyBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	// need to convert the body to a string so it is human readable
	bodyString := string(bodyBytes)

	c.JSON(http.StatusOK, gin.H{
		"body":          bodyString,
		"requestUrl":    resp.Request.URL,
		"serverHeaders": resp.Header,
		"clientHeaders": c.Request.Header,
		"clientIP":      c.ClientIP(),
		"statusCode":    resp.StatusCode,
	})
}

type Site struct {
	Address string `json:"address" binding:"required"`
}

type Hostname struct {
	Hostname string `json:"hostname" binding:"required"`
}

// run the server and handle request routing
func main() {
	gin.SetMode(gin.ReleaseMode)
	router := gin.Default()
	router.POST("/lookup", hostLookup)
	router.POST("/urlcheck", urlCheck)
	router.GET("/clientinfo", clientInfo)
	err := router.Run(":8080")
	if err != nil {
		return
	}
}

