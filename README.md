## Table of contents
* [General Info](#general-info)
* [Usage](#usage)
* [Technologies](#technologies)
* [Setup](#setup)

## General Info
This is a utility service that can come in handy when troubleshooting network and http issues 
when direct access to the runtime environment is limited.  Once deployed you can get information from the runtimes
point of view on the client, its outbound view of DNS , and its outbound view of URL's.  This comes in handy when
trying to understand if something is inserting headers between your client and the runtime environment and also when
trying to determine what outbound connectivity is like.

## Usage
There are multiple URI's defined for this API, two of them take JSON data via POST and one responds to GET requests.
All endpoints return JSON responses back to the client making this suitable for usage via CLI tools like curl or from
within other pieces of code.

```
POST /lookup
POST /urlcheck
GET /clientinfo
```

lookup is used to perform a DNS query for the A record of the provided hostname

The lookup endpoint takes a JSON array with a single k/v `hostname` and returns a JSON array with a k/v that contains
the hostname the DNS lookup was performed on and a list of the returned IP's.

urlcheck is used to gather information about the provided URL

The urlcheck endpoint takes a json array with a single k/v `address` and returns a JSON array with multiple k/v's
containing the following information :

The body of the site \
The request URL \
The server headers \
The client headers \
The client IP \
The response code 

clientinfo is used to return basic information on client connection

The clientinfo endpoint responds to GET requests with a JSON array containing the following information :

The client IP \
The request headers

Here are some examples of using curl to interact with the different endpoints 

```
prev@raptor:~$ curl http://localhost:8080/lookup \
-H "Content-Type: application/json" \
-d '{ "hostname":"example.com" }' \
-w "\n"
{"example.com":["93.184.216.34","2606:2800:220:1:248:1893:25c8:1946"]}
prev@raptor:~$
```

In the following example the JSON response is piped into jq for readability due to the inclusion of the body
```
prev@raptor:~$ curl http://localhost:8080/urlcheck \
-H "Content-Type: application/json" \
-d '{ "address":"https://example.com" }' \
-s \
| jq
{
  "body": "<!doctype html>\n<html>\n<head>\n    <title>Example Domain</title>\n\n    <meta charset=\"utf-8\" />\n    <meta http-equiv=\"Content-type\" content=\"text/html; charset=utf-8\" />\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />\n    <style type=\"text/css\">\n    body {\n        background-color: #f0f0f2;\n        margin: 0;\n        padding: 0;\n        font-family: -apple-system, system-ui, BlinkMacSystemFont, \"Segoe UI\", \"Open Sans\", \"Helvetica Neue\", Helvetica, Arial, sans-serif;\n        \n    }\n    div {\n        width: 600px;\n        margin: 5em auto;\n        padding: 2em;\n        background-color: #fdfdff;\n        border-radius: 0.5em;\n        box-shadow: 2px 3px 7px 2px rgba(0,0,0,0.02);\n    }\n    a:link, a:visited {\n        color: #38488f;\n        text-decoration: none;\n    }\n    @media (max-width: 700px) {\n        div {\n            margin: 0 auto;\n            width: auto;\n        }\n    }\n    </style>    \n</head>\n\n<body>\n<div>\n    <h1>Example Domain</h1>\n    <p>This domain is for use in illustrative examples in documents. You may use this\n    domain in literature without prior coordination or asking for permission.</p>\n    <p><a href=\"https://www.iana.org/domains/example\">More information...</a></p>\n</div>\n</body>\n</html>\n",
  "clientHeaders": {
    "Accept": [
      "*/*"
    ],
    "Content-Length": [
      "35"
    ],
    "Content-Type": [
      "application/json"
    ],
    "User-Agent": [
      "curl/7.74.0"
    ]
  },
  "clientIP": "::1",
  "requestUrl": {
    "Scheme": "https",
    "Opaque": "",
    "User": null,
    "Host": "example.com",
    "Path": "",
    "RawPath": "",
    "ForceQuery": false,
    "RawQuery": "",
    "Fragment": "",
    "RawFragment": ""
  },
  "serverHeaders": {
    "Accept-Ranges": [
      "bytes"
    ],
    "Age": [
      "384358"
    ],
    "Cache-Control": [
      "max-age=604800"
    ],
    "Content-Type": [
      "text/html; charset=UTF-8"
    ],
    "Date": [
      "Fri, 25 Jun 2021 20:46:08 GMT"
    ],
    "Etag": [
      "\"3147526947\""
    ],
    "Expires": [
      "Fri, 02 Jul 2021 20:46:08 GMT"
    ],
    "Last-Modified": [
      "Thu, 17 Oct 2019 07:18:26 GMT"
    ],
    "Server": [
      "ECS (nyb/1D13)"
    ],
    "Vary": [
      "Accept-Encoding"
    ],
    "X-Cache": [
      "HIT"
    ]
  },
  "statusCode": 200
}
prev@raptor:~$ 
```

```
prev@raptor:~$ curl http://localhost:8080/clientinfo \
-w "\n"
{"clientIP":"::1","requestHeaders":{"Accept":["*/*"],"User-Agent":["curl/7.74.0"]}}
prev@raptor:~$ 
```

## Technologies
showme is written in golang and leverages the [gin API development](https://github.com/gin-gonic/gin) framework to 
handle routing and dealing with JSON. It is meant to be containerized and deployed to kubernetes clusters in order to 
aid in troubleshooting and validation work, but equally it can be compiled and run on a standalone VM without issue.

## Setup
You can use the included dockerfile to build a container image of showme, by default it listens on all interfaces at 
tcp port 8080 for incoming requests ( this can be modified by changing the port number in the main() function ).
The dockerfile that is provided uses a multistage build and leverages the [Distroless](https://github.com/GoogleContainerTools/distroless) base image
as a runtime for the binary that gets built.

You can execute the dockerfile like this 
```
docker build -t remote-visibility -f Dockerfile .
```
And then run the container it builds like this 
```
docker run -d -p 8080:8080 remote-visibility
